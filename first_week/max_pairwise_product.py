# Uses python3

import sys
import numpy as np 


n = int(input())
a = [int(x) for x in raw_input().split()]


# find 2 maximal numbers

assert(len(a) == n)


result = 0


# def generator():
# 	a = []
# 	x = np.random.randint(1,100)
		
# 	a = np.random.randint(0,100000, x)

# 	return a

# a = generator()
# print a



def MaxPairwiseProductFast(a):

	max_index1 = -1

	# find max max num
	for x in a:
		if x > max_index1:
			max_index1 = x
		else:
			continue

	max_index2 = -1

	for x in a:
		if x > max_index2 and x < max_index1:
			max_index2 = x
		else:
			continue

	# print (max_index1, max_index2)
	result = max_index1 * max_index2

	return result
print MaxPairwiseProductFast(a)

