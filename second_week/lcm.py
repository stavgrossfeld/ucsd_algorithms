#python2
def gcd(a,b):
	# print a,b
	if b == 0:
		return a
	else:
		a_prime = a % b 
	return gcd(b,a_prime)



digs = raw_input().split()

print (int(digs[0])*int(digs[1])) / (gcd(int(digs[0]), int(digs[1])))